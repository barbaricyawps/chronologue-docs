# Chronologue documentation

This directory contains the Chronologue documentation.
To document Chronologue, we use the static site generator [HUGO](https://gohugo.io/) along with the [Doks theme](https://getdoks.org/).

## Directory structure in a nutshell
```
root
├── archetypes
├── assets
├── config
├── content/en/docs
│   ├── API
│   │   └── Documentation files for the API
│   │   └── _index.md
│   |   Documentation files
│   │   _index.md
├── data
├── functions
├── i18n
├── images
├── layouts
├── README.md
└── static

```
- `archetypes`: Contains templates for several page types. There's 3 archetypes: docs, default, and blog (which we don't use here)
- `assets`: Contains files to reference in a markdown page, such as images. Add your images here.
- `config`: Contains site-wide configuration files for menus, page metadata, etc.
- `content/en/docs/`: Contains the Markdown files for the docs. `_index.md` pages are the homepages.
- `data`: Currently contains a versioning file, but we don't use this feature.
- `functions`: We don't use functions.
- `i18n`: Contians configuration files for different languages. We currently don't translate content, so this is not relevant.
- `images`: Unclear why a second imahe folder. Use assets folder to upload your images. 
- `layouts`: Contains the layout files for the three content archetypes.
- `static`: Contains folders for styling (css, fonts, etc.), images, videos. Any contents inside here will be copied into the root of the final build directory.



## Page metadata

The Chronologue documentation pages need the following metadata (based on the docs archetype):

```
---
title: "Title"
description: "Description (optional). Shows up as a snippet on Google search result."
lead: ""
date: 2022-11-07T10:00:32+01:00
lastmod: 2022-11-07T10:00:32+01:00
draft: true
images: []
menu:
  docs:
    parent: ""
    identifier: ""
weight: 999
toc: true
---

```

- `title`: Page title. Is used to display the page's title and populates the HTML head.
- `description`: Metadata for SEO optimization.
- `lead`: Description
- `date`: Creation date
- `lastmod`: Date when content was last modified.
- `draft`: Can be `true` or `false`. When `true`, the page is excluded from the build process, and doesn't show up on the website.
- `images[]`: Unclear why the array would be needed. @bwklein, do you have more ideas?
- `menu`: The menu metadata lets you configure where a page shows up. For example, the `docs` menu is the left navigation. For all valid values, check out the file: `config/_default/menus/menus.en.toml` and the [Add menus tutorial](https://getdoks.org/docs/tutorial/add-menus/)
- `weight`: A number that determines how "high up", or leftmost a navigation item is. The smaller the weight, the more the content shows up top. In the documentation, we use weights with two digits to grant us some flexibility. For example, the first entry has a weight of `10`, the second entry a weight of `20`. This way, we could slip in another item in between, for example weight `11`.
- `toc`: Can be `true` or `false`. When `true`, the page's table of content is displayed.



## How to add contribute documentation to Chronologue

There are two types of workflows you can use to contribute documentation:

-   [Directly in Github](#Gitlab): Easy method if you just want to update text.
-   [Using Gitpod](#Gitpod): Recommended if you can use VS code and want to edit more comfortably.

### Before you contribute: Docs workflow

The documentation for Chronologue is created and maintained on the `main` branch. We use branch protection rules which are visible to administrators in the repo settings. Because of these branch protection rules, you can't directly commit onto the `main` branch. The general authoring workflow looks like this:

1. Create a new branch
2. Make your changes, for example: create a draft.
3. Open a merge request, so others can review your content.
4. When you and your reviewers agree that your documentation is ready for publication, you can merge your branch with `main`. This requires at least **one** approval.
   > ✔️ Once your PR is merged, your documentation is online.


### Contributing via Gitlab<a id="Gitlab"></a>

!The following content is old (from our Github days)!

Writing or editing files directly on Gitlab is a good way to get started if you haven't worked with the command line or IDEs or just want to make a quick change.

#### Creating a new branch:

1. Pick an issue you want to work on.
2. Use _Create a branch_ to create a new branch that is linked to the issue. <br/>
   <img width="252" alt="image" src="https://user-images.githubusercontent.com/13576110/172503283-338f72a5-29c4-4131-b159-040a35c5cffe.png">
3. Change the branch source to `docs` and name the branch how you prefer. The default is the number and name of the issue.<br/>
   <img width="500" alt="branch" src="https://user-images.githubusercontent.com/13576110/172506197-58ffe5c5-d356-444b-aa6d-a9629897350e.png">
   > ℹ️ After you click _Create branch_, a new window pops up with information that you can use in your local development setup. Since you want to work on the website, just dismiss this window.
4. Click on the branch to switch to you working branch. <br/>
   <img width="332" alt="image" src="https://user-images.githubusercontent.com/13576110/172505635-c3197f8c-eef2-42a5-9297-766e34e85b81.png">


#### To edit a file in Gitlab:

1. In your working branch, locate the file you want to edit.
2. Verify that you are in your working branch. <br/>
   <img width="600" alt="branch-overview" src="https://user-images.githubusercontent.com/13576110/172507864-b76f3163-f084-48cc-831e-233779c76934.png">
3. Click the _Edit this file_ button.<br/>
   <img width="429" alt="image" src="https://user-images.githubusercontent.com/13576110/172506536-94669369-a5a3-4b2a-9aa5-66eeeebf2442.png">
4. Edit the file using standard Markdown syntax. To see a preview, switch to the _Preview_ tab. <br/>
   <img width="291" alt="image" src="https://user-images.githubusercontent.com/13576110/172506797-4cb454e4-8c4c-4db9-85fc-7ebe9568c0c9.png">
5. Add a meaningful commit message. Remember: We are open source and your changes are visible to the public. You can commit directly to your working branch.<br/>
   <img width="738" alt="image" src="https://user-images.githubusercontent.com/13576110/172507525-a51253e3-0bc1-41c3-9c57-1ae196af92b0.png">
   > ✔️ Congrats, you just created a **commit**.

#### Creating a new merge request (MR):
1. Navigate to the tree view of your working branch. The URL should look like this:
   ```
   https://github.com/thegooddocsproject/chronologue/tree/your-branch-name
   ```
2. Click on _Contribute_ and use the _Compare_ option. <br/>
   <img width="726" alt="Go to file" src="https://user-images.githubusercontent.com/13576110/172509325-698203d3-76e9-4065-a481-df432ae01113.png">
3. Because we want to merge our changes in the `docs` branch, make sure that your changes are compared `docs` and **not** `main`. <br/>
   Click _Create pull request_.
<img width="1000" alt="Comparing changes" src="https://user-images.githubusercontent.com/13576110/172508715-fc158257-e90c-4dbf-bb97-5f5182ddf644.png">
4. Describe the changes you made and assign me (@kickoke) as a reviewer. Create the PR.
   > ℹ️ After you created your MR, @kickoke or someone who got assigned will review your changes. When you have at least **one** approval, your contribution gets merged into `main` and your content is online. ✨

### Contributing via Gitpod<a id="Gitpod"></a>
👷 Work in progress

## EkLine Integration
We have integrated EkLine into our repository to review and improve the documentation quality. When you submit a merge request, EkLine will automatically analyze the changes and provide comments and suggestions based on best practices, such as the Google Style Guide.

If you need support or have questions about the EkLine integration, please refer to the [EkLine Documentation](https://ekline.notion.site/EkLine-Documentation-820e545d76214d9d9cb2cbf627c19613)
