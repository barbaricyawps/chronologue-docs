---
title: Adding new entries to the Chronologue catalog
layout: docs
draft: true
menu:
  docs:
    parent: "API"
    identifier: "Adding new entries to the Chronologue catalog"
weight: 20
---

Explains how a developer uses a POST request to add more information.