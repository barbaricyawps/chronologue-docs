---
title: Chronologue user guide
description: "The end user documentation for the Chronologue telescope."
lead: ""
date: 2022-11-07T10:00:32+01:00
lastmod: 2022-11-07T10:00:32+01:00
draft: false
images: []
menu:
  docs:
    identifier: "User documentation"
weight: 100
toc: true
---

Welcome page.
Ask UX designers how we can make this visually appealing.